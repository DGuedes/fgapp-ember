import { test } from 'qunit';
import moduleForAcceptance from '../helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | articles');

test('visiting /news page', function(assert) {
  server.createList('article', 10);
  visit('/news');

  return andThen(() => {
    assert.equal(find('.card').length, 5);
  });
});

test('visiting article#show page', function(assert) {
  server.createList('article', 10);
  visit('/news');
  click('.card-action a');
  return andThen(() => {
    assert.equal(currentPath(), 'article.index');
    assert.equal(find('p.created-at-format').length, 1);
    assert.equal(find('p.created-at-format').text(), '22 de Outubro de 2014');
  });
});
