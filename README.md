# Fgapp

This README outlines the details of collaborating on this Ember application.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)

* [Node.js](http://nodejs.org/) (with NPM)
  Version 6.4.0. You can install it with nvm with the following command: `nvm install 6.4.0`

* [Bower](http://bower.io/)
  You can install it with the following command: `npm install -g bower`

* [Ember CLI](http://ember-cli.com/)
  You can install it with the following command: `npm install -g ember-cli`

* [Cordova](https://cordova.apache.org/)
  You can install it with the following command: `npm install -g cordova`

## Installation

* `git clone https://gitlab.com/DGuedes/fgapp.git` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember serve`
* Visit the server at [http://localhost:4200](http://localhost:4200).

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember cordova:build --platform=android`

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
