import { Serializer } from 'ember-cli-mirage';
import Ember from 'ember';
const { underscore } = Ember.String;

export default Serializer.extend({
  keyForAttribute(attr) {
    return underscore(attr);
  },

  keyForRelationship(attr) {
    return underscore(attr);
  },
});

