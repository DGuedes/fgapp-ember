import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  title(i) { return `Article ${i}`; },
  parent_id(i) {
    if (i&1) {
      return "1";
    } else {
      return "46";
    }
  },
  body(i) { return `<p>nice article ${i}`; },
  created_at(i) {
    return `2014/10/22 0${(i%8)+1}:01:31`;
  }
});
