import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  body: DS.attr('string'),
  children: DS.hasMany('article'),
  createdAt: DS.attr('date')
});
