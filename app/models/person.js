import DS from 'ember-data';

export default DS.Model.extend({
  identifier: DS.attr(),
  name: DS.attr(),
  image: DS.attr(),
  additionalData: DS.attr(),
  user: DS.attr()
});
