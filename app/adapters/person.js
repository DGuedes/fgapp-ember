import DS from 'ember-data';
import ENV from '../config/environment';

export default DS.RESTAdapter.extend({
  namespace: 'api/v1',
  buildURL(modelName, id, snapshot, requestType, query) {
    return ENV.apiBaseUrl+"/profiles/35/members";
  }
});
