import Ember from 'ember';
import DS from 'ember-data';

var underscore = Ember.String.underscore;

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
  keyForAttribute: function(attr) {
    return underscore(attr);
  },
  keyForRelationship: function(rawKey) {
    return underscore(rawKey);
  },
  attrs: {
    children: {
      embedded: 'always'
    }
  }
});
