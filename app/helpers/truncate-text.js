import Ember from 'ember';
import { parseImageLink } from '../helpers/parse-image-link';

export function truncateText(body) {
  var text = body[0];

  text = text.split('\n')[0];
  text = parseImageLink([text]);

  return text;
}

export default Ember.Helper.helper(truncateText);
