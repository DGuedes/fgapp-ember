import Ember from 'ember';
import ENV from '../config/environment';

export function parseImageLink(params) {
  var text = params[0];

  var curIdx = 0;
  var prevIdx = -1; 

  while (true) {
    curIdx = text.indexOf('img', prevIdx + 1); 
    if (curIdx === -1) {
      break;
    }

    prevIdx = curIdx;

    var first = text.substr(0, curIdx + 9); 
    var second = text.substr(curIdx + 9, text.length - curIdx);

    text = first + ENV.apiUrl + second;
  }

  return text;
}

export default Ember.Helper.helper(parseImageLink);
