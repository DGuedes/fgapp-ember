import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('professors');
  this.route('home', { path: '/' });
  this.route('news');
  this.route('write');
  this.route('search');
  this.route('contact-list');
  this.route('article', { path: '/articles/:article_id' }, function() {
  });
});

export default Router;
