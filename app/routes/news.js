import Ember from 'ember';
import moment from 'moment';

export default Ember.Route.extend({
  beforeModel() {
    moment.locale('pt-br');
  },
  model(){
    return this.store.query('article', {
      parent_id: 46,
      sort_by: 'created_at',
      per_page: 10
    });
  }
});
